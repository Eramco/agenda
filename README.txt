// Iniciar la Base de Dades

Per iniciar la bd nomes cal fer login a l'aplicació.

// Usuaris i contrasenyes

- Usuari: demo / Password: ausias
- Usuari: ausias / Password: demo
- Usuari: estudiante / Password: usuario

// Funcionalitats del programa

- Afegir contactes.
- Editar contactes.
- Visualitzar la informació del contacte fent click en el seu nom en el llistat de contactes.
- Cercar contactes per nom o nom i cognom. (no funciona possant nomes el cognom)
- Eliminar contactes.
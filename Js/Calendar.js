window.onload = function() {
	jQuery(function($) {
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '&#x3c;Ant',
			nextText: 'Sig&#x3e;',
			currentText: 'Hoy',
			monthNames: ['Gener', 'Febrer', 'Mar&ccedil;', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Septembre', 'Octubre', 'Novembre', 'Desembre'],
			monthNamesShort: ['Gen', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Des'],
			dayNames: ['Diumenge', 'Dilluns', 'Dimarts', 'Dimecres', 'Dijous', 'Divendres', 'Dissabte'],
			dayNamesShort: ['Diu', 'Dil', 'Dim', 'Dix', 'Dij', 'Div', 'Dis'],
			dayNamesMin: ['Diu', 'Dil', 'Dim', 'Dix', 'Dij', 'Div', 'Dis'],
			weekHeader: 'Sm',
			dateFormat: 'yyyy/mm/dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
	});
}

window.onload = function() {
	$(document).ready(function() {
		$("#datepicker").datepicker();
	});
}
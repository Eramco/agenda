var modal = document.getElementById('myModal');
var span = document.getElementsByClassName("close")[0];
var conf = document.getElementById('conf');
var canc = document.getElementById('canc');

var valor;

function storeValue(active){
	modal.style.display = "block";
	valor = active.id;
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
	modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}

conf.onclick = function(){
	window.location = "../PHP/DataManager/MySQLi.php?user=" + window.valor;
	modal.style.display = "none";
}

canc.onclick = function(){
	modal.style.display = "none";
}
<!DOCTYPE html>

<?php
	session_start();
	require '../PHP/DataManager/MySQLi.php'; require '../PHP/DataManager/DOM.php';
	
	if($_GET['logout']){
		session_destroy();
		setcookie(session_name(), '', time() - 3600, '/');
		echo "<script>window.location=\"../Log.html\";</script>";
	}
	
	if(!isset($_SESSION['Username'])){
		echo "<script>window.location=\"../Log.html\";</script>";
	}
	
?>

<html>
	<head>
		<meta charset = "utf-8">
		<link rel = "stylesheet" href = "../CSS/List.css">
	</head>
	<body>
		<header>
			<div id = "user_info">
				<span>
					<a class = "button" href = "../PHP/List.php?logout=1">
						<img src = "../img/shut_down_icon.png"/>
					</a>
				</span>
				<span>
					<img src = "../img/avatar_icon.png"/>
				</span>
				<span>
					<?php
						echo $_SESSION['Username'];
					?>
				</span>
			</div>
		</header>
		
		<br>

		<h1 align = "center">
			<span id = "Titulo">Contactos</span>
			<span id = "Buscador">
				<form action = <?php $_SERVER['PHP_SELF'];?>>
					<input type = "search" placeholder = "Buscar..." name = "search">
					<input type = "image" src = "../img/lupa.png" alt = "Submit" width = "21px" height = "21px">
				</form>
			</span>
		</h1>

		<ul style="list-style-type:none">
			<?php
				$contacts = getUserContacts($_GET['search'], $_SESSION['Username']);
				
				foreach($contacts as $i){
					createContactItem($i);
				}
			?>
			<div id = "addButton" align = "right">
				<a href = "../Add.html">
					<img src="../img/add_button.png" alt="">
				</a>
			</div>
		</ul>
		
		<?php
			createModal();
		?>

		<script type = "text/javascript" src = "../Js/Delete.js"></script>
	</body>
</html>
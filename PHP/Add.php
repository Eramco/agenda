<?php
	require "../PHP/DataManager/MySQLi.php";
	session_start();
	
	if(!isset($_SESSION['Username'])){
		echo "<script>window.location=\"../Log.html\";</script>";
	}
	
	if($_POST){
		$date = date_create($_POST['fecha']);
		$date = date_format($date, 'Y-m-d');
		
		$new_contact = array($_POST['nombre'], $_POST['apellidos'], $_POST['fecha'], $_POST['e-mail'], $_POST['telefono'], $_SESSION['Username']);
		insertContact($new_contact);
		
		echo "<script>".
					"window.location = \"../PHP/List.php\"".
				 "</script>";
	}
?>
<?php
	function createContactItem($info){
		$GLOBALS['document'] = new DOMDocument();
		
		$div = createDiv($info); $li = createLi(); $contact =  createContactInfo($info); 
		$user_avatar = createUserImage(); $icons = createIcons($info);

		$div->appendChild($user_avatar);		
		$div->appendChild($contact);
		
		$div->appendChild($icons);

		$listItem = $li->appendChild($div);
		$listItem->setAttribute("id", "contact");
		
		$GLOBALS['document']->appendChild($li);
		$GLOBALS['document']->formatOutput = true;
		
		print $GLOBALS['document']->saveHTML();
	}
	
	function createDiv($info){
		$div = 	$GLOBALS['document']->createElement("div");
		return $div;
	}
	
	function createLi(){
		$li = $GLOBALS['document']->createElement("li");
		
		return $li;
	}
	
	function createUserImage(){
		$span = $GLOBALS['document']->createElement("span");
		$img = $GLOBALS['document']->createElement("img");
		
		$user_avatar = $span->appendChild($img);
		$user_avatar->setAttribute("src", "../img/imagen_usuario.png");
		
		$span->setAttribute('id', 'avatar');
		
		return $span;
	}
	
	function createContactInfo($user){
		$info = $GLOBALS['document']->createElement("span");
		$name = $GLOBALS['document']->createElement("div");
		$mail = $GLOBALS['document']->createElement("div");

		$text_name = $GLOBALS['document']->createTextNode($user[1]." ".$user[2]);
		$text_mail = $GLOBALS['document']->createTextNode($user[4]);
		
		$name->appendChild($text_name);
		$mail->appendChild($text_mail);
		
		$info->appendChild($name);
		$info->appendChild($mail);
		
		$info->setAttribute('id', 'info');
		$info->setAttribute('onclick', 'window.location = "../PHP/View.php?id='.$user[0].'"');
		
		return $info;
	}
	
	function createIcons($info){
		$icons = $GLOBALS['document']->createElement("span");
		
		$edit_span = $GLOBALS['document']->createElement("span");
		$drop_span = $GLOBALS['document']->createElement("span");
		$fav_span = $GLOBALS['document']->createElement("span");
		
		$edit_a = $GLOBALS['document']->createElement("a");
		$drop_btn = $GLOBALS['document']->createElement("btn");
		
		$edit_img = $GLOBALS['document']->createElement("img");
		$drop_img = $GLOBALS['document']->createElement("img");
		$fav_img = $GLOBALS['document']->createElement("img");
		
		$edit = $edit_a->appendChild($edit_img);
		$edit->setAttribute('src', '../img/lapiz.png');
		
		$edit = $edit_span->appendChild($edit_a);
		$edit->setAttribute('href', '../PHP/Edit.php?id='.$info[0]);
		
		$drop = $drop_btn->appendChild($drop_img);
		$drop->setAttribute('src', '../img/basura.png');
		
		$drop = $drop_span->appendChild($drop_btn);
		$drop->setAttribute('id', $info[0]);
		$drop->setAttribute('onclick', 'storeValue(this)');
		
		$fav = $fav_span->appendChild($fav_img);
		$fav->setAttribute('src', '../img/corazon.png');
		
		$icons->appendChild($edit_span);
		$icons->appendChild($drop_span);
		$icons->appendChild($fav_span);
		
		$icons->setAttribute('id', 'icons');
		
		return $icons;
	}
	
	function createModal(){
		$GLOBALS['document'] = new DOMDocument();

		$modal = createModalWindow();
		$content = createModalContent();
		
		$div = $modal->appendChild($content);
		$div->setAttribute('class', 'modal-content');
		
		$div = $GLOBALS['document']->appendChild($modal);
		$div->setAttribute('id', 'myModal');
		$div->setAttribute('class', 'modal');	

		$GLOBALS['document']->formatOutput = true;
		print $GLOBALS['document']->saveHTML();
	}
	
	function createModalWindow(){
		$div = $GLOBALS['document']->createElement("div");
		
		return $div;
	}
	
	function createModalContent(){
		$div_global = $GLOBALS['document']->createElement("div");
		$div_header = $GLOBALS['document']->createElement("div");
		$div_body = $GLOBALS['document']->createElement("div");
		
		$span_header = $GLOBALS['document']->createElement("span");
		$h2_header = $GLOBALS['document']->createElement("h2");
		
		$p_body = $GLOBALS['document']->createElement("p");
		$confBtn_body = $GLOBALS['document']->createElement("button");
		$cancBtn_body = $GLOBALS['document']->createElement("button");
		
		$closeText = $GLOBALS['document']->createTextNode("×");
		$titleText = $GLOBALS['document']->createTextNode("Advertencia");
		$confirmText = $GLOBALS['document']->createTextNode("Si");
		$cancelText = $GLOBALS['document']->createTextNode("No");
		$warnText = $GLOBALS['document']->createTextNode("Segur que vols eliminar el contacte?");
		
		$span_header->appendChild($closeText);
		$h2_header->appendChild($titleText);
		
		$div = $div_header->appendChild($span_header);
		$div->setAttribute('class', 'close');
		$div_header->appendChild($h2_header);
		
		$confBtn_body->appendChild($confirmText);
		$cancBtn_body->appendChild($cancelText);
		$p_body->appendChild($warnText);
		
		$div_body->appendChild($p_body);
		$div = $div_body->appendChild($confBtn_body);
		$div->setAttribute('id', 'conf');
		$div = $div_body->appendChild($cancBtn_body);
		$div->setAttribute('id', 'canc');
		
		$div = $div_global->appendChild($div_header);
		$div->setAttribute('class', 'modal-header');
		$div = $div_global->appendChild($div_body);
		$div->setAttribute('class', 'modal-body');
		
		return $div_global;
	}
?>
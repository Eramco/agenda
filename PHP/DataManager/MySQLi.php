<?php
	startConnection();
//	insertContact(array(4, 'cont4', 'ap4', '1970/01/01', 'cont4@gmail.com', '111111111', 'ausias'));
	
	function startConnection(){
		$GLOBALS['mysqli'] = mysqli_connect('localhost', 'a14eriamocob_try', 'ausias', 'a14eriamocob_Agenda');
	}
	
	function checkDatabase(){
		if(!$GLOBALS['mysqli']) startConnection();
		
		createTables();
		insertDemoData();
	}
	
	function createTables(){
		if(!$GLOBALS['mysqli']) startConnection();
		
		$new_userTable = "CREATE TABLE IF NOT EXISTS User(".
										 "USER_NAME varchar(255) NOT NULL PRIMARY KEY,".
										 "PASSWORD char(32) NOT NULL);";

		$new_contactTable = "CREATE TABLE IF NOT EXISTS Contact(".
												"ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,".
												"NOMBRE varchar(255) NOT NULL,".
												"APELLIDOS varchar(255) NOT NULL,".
												"NACIMIENTO DATE NOT NULL,".
												"EMAIL varchar(255) NOT NULL,".
												"TELEFONO varchar(20) NOT NULL,".
												"USER varchar(255) NOT NULL,".
												"FOREIGN KEY(USER) REFERENCES User(USER_NAME));";
		
		$index_restriction = "CREATE UNIQUE INDEX no_dup ON Contact(NOMBRE, APELLIDOS, EMAIL, USER);";
		
		$GLOBALS['mysqli']->query($new_userTable);
		$GLOBALS['mysqli']->query($new_contactTable);
		$GLOBALS['mysqli']->query($index_restriction);
	}
	
	function insertDemoData(){
		$insert_user1 = "INSERT INTO User VALUES('demo', MD5('ausias'));";
		$insert_user2 = "INSERT INTO User VALUES('ausias', MD5('demo'));";
		$insert_user3 = "INSERT INTO User VALUES('estudiante', MD5('usuario'));";
		
		$GLOBALS['mysqli']->query($insert_user1);
		$GLOBALS['mysqli']->query($insert_user2);
		$GLOBALS['mysqli']->query($insert_user3);
		
		$insert_cont1 = "INSERT INTO Contact(NOMBRE, APELLIDOS, NACIMIENTO, EMAIL, TELEFONO, USER) VALUES('cont1', 'ap1', '1970/06/20', 'cont1@gmail.com', '111111111', 'demo');";
		$insert_cont2 = "INSERT INTO Contact(NOMBRE, APELLIDOS, NACIMIENTO, EMAIL, TELEFONO, USER) VALUES('cont2', 'ap2', '1970/06/20', 'cont2@gmail.com', '111111111', 'ausias');";
		$insert_cont3 = "INSERT INTO Contact(NOMBRE, APELLIDOS, NACIMIENTO, EMAIL, TELEFONO, USER) VALUES('cont3', 'ap3', '1970/06/20', 'cont3@gmail.com', '111111111', 'ausias');";
		
		$GLOBALS['mysqli']->query($insert_cont1);
		$GLOBALS['mysqli']->query($insert_cont2);
		$GLOBALS['mysqli']->query($insert_cont3);
	}

	function checkUser($name, $pass){
		checkDatabase();
		
		$user_check = "SELECT * FROM User WHERE USER_NAME = '".$name."' AND PASSWORD = MD5('".$pass."');";
		$info = $GLOBALS['mysqli']->query($user_check);
		
		if($info->num_rows == 1) return true;
		else return false;
	}

	function getUserContacts($contact_name, $user_name){
		if(!$GLOBALS['mysqli'])	startConnection();
		
		if($contact_name != ''){
			$name = split(' ', $contact_name);
			$user_contacts = "SELECT DISTINCT * FROM Contact WHERE USER = '".$user_name."' AND NOMBRE LIKE '%". $name[0]."%' AND APELLIDOS LIKE '%".$name[1]."%';";
		}
		
		else $user_contacts = "SELECT DISTINCT * FROM Contact WHERE USER = '".$user_name."' ORDER BY NOMBRE, APELLIDOS;";
		
		$users = $GLOBALS['mysqli']->query($user_contacts);
		$contacts = array();
		
		while($row = mysqli_fetch_row($users)){
			array_push($contacts, $row);
		}
		
		return $contacts;
	}
	
	function getContact($id){
		if(!$GLOBALS['mysqli'])	startConnection();
		
		$query = "SELECT * FROM Contact WHERE ID = ".$id.";";
		$contact = $GLOBALS['mysqli']->query($query);
		
  	return mysqli_fetch_row($contact);
	}
	
	function updateContact($info){
		if(!$GLOBALS['mysqli'])	startConnection();
		
		$query = "UPDATE Contact SET NOMBRE = '".$info[1]."', APELLIDOS = '".$info[2]."', NACIMIENTO = '".$info[3]."', EMAIL = '".$info[4]."', TELEFONO = '".$info[5]."' WHERE ID = ".$info[0].";";
		$GLOBALS['mysqli']->query($query);
	}
	
	function insertContact($contact){
		if(!$GLOBALS['mysqli'])	startConnection();
		
		$query = "INSERT INTO Contact(NOMBRE, APELLIDOS, NACIMIENTO, EMAIL, TELEFONO, USER) VALUES('".$contact[0]."', '".$contact[1]."', '".$contact[2]."', '".$contact[3]."', '".$contact[4]."', '".$contact[5]."');";
		$GLOBALS['mysqli']->query($query);
	}
	
	if($_GET['user']){
		if(!$GLOBALS['mysqli'])	startConnection();
		
		$query = "DELETE FROM Contact WHERE ID = ".$_GET['user'].";";
		$GLOBALS['mysqli']->query($query);

		echo "<script>window.location.replace(\"../List.php\")</script>";
	}
?>
<!DOCTYPE html>

<?php
	require "../PHP/DataManager/MySQLi.php";
	session_start();
	
	if(!isset($_SESSION['Username'])){
		echo "<script>window.location=\"../HTML/Log.html\";</script>";
	}
	
	if($_POST){
		$date = date_create($_POST['fecha']);
		$date = date_format($date, 'Y-m-d');
		
		$new_contact = array($_POST['id'], $_POST['nombre'], $_POST['apellidos'], $date, $_POST['e-mail'], $_POST['telefono']);
		updateContact($new_contact);
		
		// <!--if($_FILES["img"]["error"] == 0){-->
		// <!--	$destination = "../img";-->
		// <!--	if(move_uploaded_file($_FILES["img"]["tmp_name"], $destination."/".basename($_FILES['img']['name']))){-->
		// <!--		echo "<script>alert('okay');</script>";-->
		// <!--	}-->
			
		// <!--	else echo "<script>alert('nice try');</script>";-->
		// <!--}-->
		
		// <!--echo "<script>alert('nope');</script>";-->
		
		echo "<script>window.location = \"../PHP/List.php\"</script>";
	}
?>

<html>
	<head>
		<meta charset = "utf-8">
		<link rel = "stylesheet" href = "../CSS/Edit.css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
		 	<script type="text/javascript">
				jQuery(function($){
					$.datepicker.regional['es'] = {
						closeText: 'Cerrar',
						prevText: '&#x3c;Ant',
						nextText: 'Sig&#x3e;',
						currentText: 'Hoy',
						monthNames: ['Gener','Febrer','Mar&ccedil;','Abril','Maig','Juny','Juliol','Agost','Septembre','Octubre','Novembre','Desembre'],
						monthNamesShort: ['Gen','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Des'],
						dayNames: ['Diumenge','Dilluns','Dimarts','Dimecres','Dijous','Divendres','Dissabte'],
						dayNamesShort: ['Diu','Dil','Dim','Dix','Dij','Div','Dis'],
						dayNamesMin: ['Diu','Dil','Dim','Dix','Dij','Div','Dis'],
						weekHeader: 'Sm',
						dateFormat: 'dd/mm/yy',
						firstDay: 1,
						isRTL: false,
						showMonthAfterYear: false,
						yearSuffix: ''};
					$.datepicker.setDefaults($.datepicker.regional['es']);
				});
				
		    $(document).ready(function() {
		       $("#datepicker").datepicker();
		    });
	    </script>
	</head>
	<body>
		<header>
			<h1>
				<span>
					<a href = "../PHP/List.php">
						<img src = "../img/flecha.png">
					</a>
				</span>
				<span>
					Datos del contacto
				</span>
			</h1>
		</header>
		
		<br>
		
		<span id = "user">
			<img src = "../img/datos_contacto.png" alt="">
			<label for = "inputId">
				<image src = "../img/lapiz.png" style = "margin-left: 15px;">
			</label>
		</span>
		
		<form method = 'post' action = <?php echo $_SERVER['PHP_SELF']?>>
			<div id = 'form' align = 'right'>
				<input id="inputId" type="file" name = "img" style="position: fixed; top: -100em" accept = "image/jpg, image/png">
				<div>
		  		<span>Nom:</span>
		    	<span>
		    		<input type ='hidden' name = 'id' value = <?php echo $_GET['id']?>>
		    		<input type ='text' name = 'nombre' value = <?php $contact = getContact($_GET['id']); echo $contact[1];?>>
		    	</span>
		    </div>
		    <div>
		  		<span>Cognoms:</span>
		    	<span>
		    		<input type ='text' name = 'apellidos' value = <?php $contact = getContact($_GET['id']); echo $contact[2];?>>
		    	</span>
		    </div>
		    <div>
		  		<span>E-mail:</span>
		    	<span>
		    		<input type ='email' name = 'e-mail' value = <?php $contact = getContact($_GET['id']); echo $contact[4];?>>
		    	</span>
		    </div>
		    <div>
		  		<span>Data naixement:</span>
		    	<span>
		    		<input type="text" name="datepicker" id="datepicker" readonly="readonly" size="12" value = 	<?php $contact = getContact($_GET['id']); echo $contact[3];?>>
		    	</span>
		    </div>
		    <div>
		  		<span>Telefon:</span>
		    	<span>
		    		<input type ='tel' name = 'telefono' value = <?php $contact = getContact($_GET['id']); echo $contact[5];?>>
		    	</span>
		    </div>
		    <div>
		 			<input type="image" src="../img/tick_verde.png" alt="Submit">
		    </div>
			</div>
		</form>
	</body>
</html>
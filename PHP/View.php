<!DOCTYPE html>

<?php
	require "../PHP/DataManager/MySQLi.php";
	session_start();
	
	if(!isset($_SESSION['Username'])){
		echo "<script>window.location=\"../HTML/Log.html\";</script>";
	}
?>

<html>
	<head>
		<meta chraset = "utf-8">
		<link rel = "stylesheet" href = "../CSS/View.css">
	</head>
	<body>
		<header>
			<h1>
				<span>
					<a href = "../PHP/List.php">
						<img src = "../img/flecha.png">
					</a>
				</span>
				<span>
					Datos del contacto
				</span>
			</h1>
		</header>
			
		<br>
		
		<span id = "user">
			<img src = "../img/datos_contacto.png" alt="">
		</span>
		<div id = 'form' align = 'rigth'>
		    <div align = 'right'>
          		<span>Nom:</span>
            	<span>
            		<label>
            			<?php
            				$contact = getContact($_GET['id']);
                            echo $contact[1];    				
            			?>
            		</label>
            	</span>
            </div>
            <div align = 'right'>
          		<span>Cognoms:</span>
            	<span>
            	   	<label>
            			<?php
            				$contact = getContact($_GET['id']);
                            echo $contact[2];    				
            			?>
            		</label>
            	</span>
            </div>
            <div align = 'right'>
           		<span>E-mail:</span>
            	<span>
            	   	<label>
            			<?php
            				$contact = getContact($_GET['id']);
                            echo $contact[4];    				
            			?>
            		</label>
            	</span>
            </div>
            <div align = 'right'>
          		<span>Data naixement:</span>
            	<span>
            	   	<label>
            			<?php
            				$contact = getContact($_GET['id']);
                            echo $contact[3];    				
            			?>
            		</label>
            	</span>
            </div>
            <div align = 'right'>
          	    <span>Telefon:</span>
            	<span>
            	   	<label>
            			<?php
            				$contact = getContact($_GET['id']);
                            echo $contact[5];    				
            			?>
            		</label>
            	</span>
            </div>
		</div>
    </body>
</html>